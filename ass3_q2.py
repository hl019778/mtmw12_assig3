import numpy as np
import matplotlib.pyplot as plt
from defi import PhysProps
from defi import pressure
from defi import uGeoExact
from defi import DpDy_2order
from defi import u_2order
from defi import dpdy_nm_appro
from defi import u_nm_appro
from defi import figure_plot

### main code

def nm_approx():
    
    N = 100
    ymax = PhysProps["ymax"]
    ymin = PhysProps["ymin"]
    dy_10 = (ymax - ymin)/N
    y_10 = np.linspace(ymin, ymax, N+1)
    p_10 = pressure (y_10, PhysProps)
    uexact_10 = uGeoExact (y_10, PhysProps)

    ### 2nd_order
    dpdy_10 = DpDy_2order(N,p_10,dy_10)
    u_10 = u_2order(dpdy_10,PhysProps)
    err_10 = u_10 - uexact_10

    ### another method
    dpdy_10_nm = dpdy_nm_appro(N,p_10,dy_10)
    u_10_nm = u_nm_appro(dpdy_10_nm,PhysProps)
    err_10_nm = u_10_nm - uexact_10
    print ("the average error for the 2-point method is",np.mean(err_10[1::]),"and the error for the 3-point method is",np.mean(err_10_nm[1::]))
    figure_plot(y_10,uexact_10,u_10_nm,N)

    ### plot figure
    plt.plot(y_10, err_10, label = "2-point")
    plt.plot(y_10, err_10_nm,"--", label = "3-point")
    plt.xlabel("y(m)")
    plt.ylabel("Error(m/s)")
    plt.legend()
    plt.title("2-point and 3-point approximation of wind speed for N = 100")
    plt.show()
nm_approx()

def error_reduce():
    err = np.zeros(20)
    dy = np.zeros(20)
    for i in range (20):
        N = 10*(i+1)
        ymax = PhysProps["ymax"]
        ymin = PhysProps["ymin"]
        dy[i] = (ymax - ymin)/N
        y = np.linspace(ymin, ymax, N+1)
        dpdy = dpdy_nm_appro(N,pressure (y, PhysProps),dy[i])
        e = u_nm_appro(dpdy,PhysProps)-uGeoExact (y, PhysProps)
        err[i] = np.mean(e[0:-2])
    plt.plot(dy, np.abs(err))
    plt.xlabel("delty")
    plt.ylabel("mean value of error")
    plt.title("how error changes with delty")
    plt.show()
    plt.plot(dy[0]/dy, err[0]/err, label = "experiment")
    plt.plot(dy[0]/dy, (dy[0]/dy)**2,"--", label = "theoritical expection")
    plt.legend()
    plt.xlabel("How many times dy decreased")
    plt.ylabel("How many times error decreased")
    plt.title("how error decreases with delty decreasing")
    plt.show()
error_reduce()
