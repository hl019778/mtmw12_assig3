import numpy as np
import matplotlib.pyplot as plt

### Physical parameters
PhysProps = {'pa' : 1e5,                               ### mean pressure
             'pb' : 200.,                              ### magnitude of pressure variation
             'f' : 1e-4,                               ### Coriolis parameter
             'rho' : 1.,                               ### density
             'L' : 2.4e6,                              ### length scale
             'ymin' : 0.,                              ### start of y domain
             'ymax' : 1e6,                             ### End of y domain
             }

### numerically differenciating the pressure
def pressure (y, props):
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    p = pa + pb*np.cos(y*np.pi/L)
    return (p)

def uGeoExact (y, props):
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    rho = props["rho"]
    f = props["f"]
    uexact = pb*np.pi*np.sin(y*np.pi/L)/(rho*f*L)
    return (uexact)

def DpDy_2order(N,p,dy):
    dpdy = np.zeros(N+1)
    dpdy[0] = (p[1] - p[0])/dy
    dpdy[N] = (p[N] - p[N-1])/dy
    for i in range (1,N):
        dpdy[i] = (p[i+1] - p[i-1])/(2*dy)
    return dpdy

def u_2order(dpdy,props):
    rho = props["rho"]
    f = props["f"]    
    u = -dpdy/(rho*f)
    return (u)

def figure_plot(y,uexact,u,N):
    plt.plot(y, uexact, label = "Exact u")
    plt.plot(y, u, "-.", label = "Numerically u")
    plt.legend()
    plt.title("Exact and approximated wind speed for N ="+str(N))
    plt.xlabel("y(m)")
    plt.ylabel("u(m/s)")
    plt.show()
    return ()

def error(uexact,u):
    err = u - uexact
    return (err)

def error_plot(y,err):
    plt.plot(y, err, "-.")
    plt.xlabel("y(m)")
    plt.ylabel("u(m/s)")
    plt.title("Errors of numerically solution")
    plt.show()
    return ()

def dpdy_nm_appro(N,p,dy):
    dpdy = np.zeros(N+1)
    dpdy[N-1] = (p[N] - p[N-2])/(2*dy)
    dpdy[N] = (p[N] - p[N-1])/dy
    for i in range (0,N-1):
        dpdy[i] = (-p[i+2] + 4*p[i+1] - 3*p[i])/(2*dy)
    return dpdy

def u_nm_appro(dpdy,props):
    rho = props["rho"]
    f = props["f"]    
    u = -dpdy/(rho*f)
    return (u)

