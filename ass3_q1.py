import numpy as np
import matplotlib.pyplot as plt
from defi import PhysProps
from defi import pressure
from defi import uGeoExact
from defi import DpDy_2order
from defi import u_2order
from defi import figure_plot
from defi import error_plot

### main code
def main():
    N = 10
    ymax = PhysProps["ymax"]
    ymin = PhysProps["ymin"]
    dy_10 = (ymax - ymin)/N
    y_10 = np.linspace(ymin, ymax, N+1)
    p_10 = pressure (y_10, PhysProps)
    uexact_10 = uGeoExact (y_10, PhysProps)
    dpdy_10 = DpDy_2order(N,p_10,dy_10)
    u_10 = u_2order(dpdy_10,PhysProps)
    figure_plot(y_10,uexact_10,u_10,N)
    err_10 = u_10-uexact_10
    error_plot(y_10,err_10)
main()

def error_reduce():
    err = np.zeros(20)
    dy = np.zeros(20)
    for i in range (20):
        N = 10*(i+1)
        ymax = PhysProps["ymax"]
        ymin = PhysProps["ymin"]
        dy[i] = (ymax - ymin)/N
        y = np.linspace(ymin, ymax, N+1)
        dpdy = DpDy_2order(N,pressure (y, PhysProps),dy[i])
        e = u_2order(dpdy,PhysProps)-uGeoExact (y, PhysProps)
        err[i] = np.mean(e[1:-2])
    plt.plot(dy, np.abs(err))
    plt.xlabel("delty")
    plt.ylabel("mean value of error")
    plt.title("how error changes with delty")
    plt.show()
    plt.plot(dy[0]/dy, err[0]/err, label = "experiment")
    plt.plot(dy[0]/dy, (dy[0]/dy)**2,"--", label = "theoritical expection")
    plt.legend()
    plt.xlabel("How many times dy decreased")
    plt.ylabel("How many times error decreased")
    plt.title("how error decreases with delty decreasing")
    plt.show()
error_reduce()
